FROM docker.io/library/ubuntu:jammy

ENV DEBIAN_FRONTEND=noninteractive
ENV PKG_DEPS="\
    ca-certificates \
    curl \
    gnupg \
    tzdata \
    sudo \
"

# Can be overriden at build time
ARG CI_RUNNER_PASSWORD=ci-runner

COPY morello-*.install /tmp/

RUN set -e ;\
    bash -ex /tmp/morello-environment.install ;\
    . /etc/environment ;\
    echo 'dash dash/sh boolean false' | debconf-set-selections ;\
    echo "${TZ}" > /etc/timezone ;\
    (cd /etc ; ln -sf /usr/share/zoneinfo/${TZ} localtime) ;\
    apt update -q=2 ;\
    apt full-upgrade -q=2 --yes ;\
    apt install -q=2 --yes --no-install-recommends ${PKG_DEPS} ;\
    echo "deb ${KUBIC_REPO}/ /" > /etc/apt/sources.list.d/kubic.list ;\
    curl -sL ${KUBIC_REPO}/Release.key | apt-key add - ;\
    apt update -q=2 ;\
    apt install -q=2 --yes --no-install-recommends podman buildah runc ;\
    # Set default shell to bash
    dpkg-reconfigure -p critical dash ;\
    # Set timezone
    dpkg-reconfigure -p critical tzdata ;\
    # Setup ci-runner user
    useradd -m -s /bin/bash ci-runner ;\
    echo "ci-runner:$CI_RUNNER_PASSWORD" | chpasswd ;\
    echo 'ci-runner ALL = NOPASSWD: ALL' > /etc/sudoers.d/ci ;\
    chmod 0440 /etc/sudoers.d/ci ;\
    # Cleanup
    apt clean ;\
    rm -rf /var/lib/apt/lists/* /tmp/*

USER ci-runner

WORKDIR /home/ci-runner
CMD ["/bin/bash"]
