FROM docker.io/library/ubuntu:focal

ENV DEBIAN_FRONTEND=noninteractive
ENV TOOLS_DIR=/home/ci-runner/tools
ENV PATH=${TOOLS_DIR}/bin:${TOOLS_DIR}/arm_gcc/bin:${TOOLS_DIR}/linaro_gcc/bin:${TOOLS_DIR}/clang/bin:${PATH}
ENV LANG=en_US.UTF-8
ENV PKG_DEPS="\
    acpica-tools \
    automake \
    autopoint \
    binutils-aarch64-linux-gnu \
    bison \
    build-essential \
    ca-certificates \
    curl \
    device-tree-compiler \
    doxygen \
    flex \
    gettext-base \
    git \
    jq \
    less \
    libssl-dev \
    locales \
    openssh-client \
    pkg-config \
    python3 \
    python3-distutils \
    sudo \
    uuid-dev \
    wget \
    xmlstarlet \
    xz-utils \
    zip \
    unzip \
    libncurses5 \
"

# Can be overriden at build time
ARG CI_RUNNER_PASSWORD=ci-runner

COPY morello-*.install /tmp/

RUN set -e ;\
    echo 'locales locales/locales_to_be_generated multiselect C.UTF-8 UTF-8, en_US.UTF-8 UTF-8 ' | debconf-set-selections ;\
    echo 'locales locales/default_environment_locale select en_US.UTF-8' | debconf-set-selections ;\
    echo 'dash dash/sh boolean false' | debconf-set-selections ;\
    apt update -q=2 ;\
    apt full-upgrade -q=2 --yes ;\
    apt install -q=2 --yes --no-install-recommends ${PKG_DEPS} ;\
    # Set default shell to bash
    mkdir -p /usr/share/man/man1 ;\
    dpkg-reconfigure -p critical dash ;\
    # Set Python 3 as default
    update-alternatives --install /usr/bin/python python /usr/bin/python3 50 ;\
    # Setup ci-runner user
    useradd -m -s /bin/bash ci-runner ;\
    echo "ci-runner:$CI_RUNNER_PASSWORD" | chpasswd ;\
    echo 'ci-runner ALL = NOPASSWD: ALL' > /etc/sudoers.d/ci ;\
    chmod 0440 /etc/sudoers.d/ci ;\
    # Run shell script(s) to install files, toolchains, etc...
    mkdir -p ${TOOLS_DIR}/bin ;\
    bash -ex /tmp/morello-dependencies.install ;\
    bash -ex /tmp/morello-environment.install ;\
    # Fix permissions
    chown -R ci-runner:ci-runner ${TOOLS_DIR} ;\
    # Cleanup
    apt clean ;\
    rm -rf /var/lib/apt/lists/* /tmp/*

USER ci-runner

RUN set -e ;\
    # Set git default config
    git config --global user.email "ci@morello-project.org" ;\
    git config --global user.name "Morello CI" ;\
    git config --global color.ui "auto" ;\
    echo "progress = dot" > ${HOME}/.wgetrc ;\
    echo "dot_bytes = 10m" >> ${HOME}/.wgetrc

WORKDIR /home/ci-runner
CMD ["/bin/bash"]
