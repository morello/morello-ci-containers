#!/bin/sh

set -e

buildah login -u ${CI_REGISTRY_USER} -p ${CI_REGISTRY_PASSWORD} ${CI_REGISTRY}

publish_model()
{
  test -z "${AWS_REGISTRY_PASSWORD}" && return 0
  FVP_VERSION="${FVP_VERSION:-"0.11.16"}"
  tag="${AWS_REGISTRY}/fvp:${FVP_VERSION}"
  set +x
  echo "${AWS_REGISTRY_PASSWORD}" | buildah login -u AWS --password-stdin "${AWS_REGISTRY}"
  set -x
  buildah tag "${tag1}" "${tag}"
  buildah push "${tag}"
}

set -x

arch=$(dpkg --print-architecture)
date=$(date +%Y%m%d-%H%M%S)
for module in ${CONTAINER}; do
  if [ ${module} = "buildah" ] || [ ${module} = "lava" ] || [ ${module} = "toolchain" ] || [ ${module} = "sdk" ] ; then
    suffix=".${arch}"
  fi
  tag1="${CI_REGISTRY_IMAGE}/morello-${module}:${CI_COMMIT_REF_NAME}.${CI_PIPELINE_ID}${suffix}"
  tag2="${CI_REGISTRY_IMAGE}/morello-${module}:${date}${suffix}"
  if [ ${CI_COMMIT_REF_NAME} = "main" ]; then
    tag3="${CI_REGISTRY_IMAGE}/morello-${module}:latest${suffix}"
  else
    tag3="${CI_REGISTRY_IMAGE}/morello-${module}:${CI_COMMIT_REF_NAME}.latest${suffix}"
  fi
  buildah bud --storage-driver=vfs --tag "${tag1}" --tag "${tag2}" --tag "${tag3}" --squash ${module}/
  # Skip model publishing unless we use a private registry like ECR.
  # It isn't allowed to redistribute the model to a public registry.
  if [ ${module} = "model" ]; then
    publish_model
    continue
  fi
  buildah push ${tag1}
  buildah push ${tag2}
  buildah push ${tag3}
done

buildah images
