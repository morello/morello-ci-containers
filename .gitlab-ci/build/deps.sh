
#!/bin/sh

set -e

TZ=Europe/London
echo "${TZ}" > /etc/timezone
(cd /etc ; ln -sf /usr/share/zoneinfo/${TZ} localtime)

apt update -q=2
apt full-upgrade -q=2 --yes
apt install -q=2 --yes --no-install-recommends ca-certificates curl gnupg tzdata
dpkg-reconfigure -p critical tzdata

KUBIC_REPO="https://mirrorcache.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/xUbuntu_21.10"
echo "deb ${KUBIC_REPO}/ /" > /etc/apt/sources.list.d/kubic.list
curl -sL ${KUBIC_REPO}/Release.key | apt-key add -

apt update -q=2
apt install -q=2 --yes --no-install-recommends podman buildah runc

